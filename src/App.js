import React, { Component } from 'react';
import './App.css';

import DataComponents from './components/datacomponents';
import Header from './components/header';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header title="Ride Stats"/>
        <DataComponents />
      </div>
    );
  }
}

export default App;
