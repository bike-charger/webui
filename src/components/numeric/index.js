import React, { Component } from 'react';
import './Numeric.css';

class Numeric extends Component {
    render() {

        var val = this.props.value;
        if (this.props.decimals) {
            val = Math.floor(val * Math.pow(10, this.props.decimals)) / Math.pow(10, this.props.decimals)
        }

        return(
            <div className='Numeric' style={{width: this.props.width, height: this.props.height}}>
                <div className='numeric-centerfix'></div>
                <div className='numeric-main'>{val.toFixed(this.props.decimals)}</div><div className='numeric-units'>{this.props.units}</div>
                <p className='numeric-measurement'>{this.props.measurement}</p>
            </div>
        );
    }
}

export default Numeric;