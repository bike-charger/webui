import React, { Component } from 'react';
import './Header.css';

class Header extends Component {
    render() {

        var comp = <h1>{this.props.title}</h1>;
        if (this.props.small)
            comp = <h3>{this.props.title}</h3>;

        return(
            <div className="Header">
                {comp}
            </div>
        );
    }
}

export default Header;