import React, { Component } from 'react';
import './Text.css';

class Numeric extends Component {
    render() {
        return(
            <div className='Text' style={{width: this.props.width, height: this.props.height}}>
                <div className='text-centerfix'></div>
                <div className='text-main'>{this.props.value}</div>
                <p className='text-measurement'>{this.props.measurement}</p>
            </div>
        );
    }
}

export default Numeric;