import React, { Component } from 'react';
import { RadialGauge } from 'react-canvas-gauges';

class Guage extends Component {
    render() {

        var maxValue = this.props.maxValue;
        if (maxValue % this.props.stepValue !== 0) {
            maxValue += this.props.stepValue - (maxValue % this.props.stepValue);
        }

        var majorTicks = [];
        for (let i = 0; i <= maxValue; i+=this.props.stepValue) {
            majorTicks.push(i);
        }

        return (
            <RadialGauge
            units={this.props.units}
            title={this.props.title}
            value={this.props.value}
            minValue={0}
            maxValue={maxValue}
            majorTicks={majorTicks}
            minorTicks={this.props.stepValue / this.props.subInterval}
            highlights={[]}
            width={this.props.width}
            height={this.props.height}
             />
        );
    }
}

export default Guage;