import React, { Component } from 'react';

import Gauge from '../gauge';
import Numeric from '../numeric';
import Text from '../text';

const timeInterval = 100;

class DataComponents extends Component {

    constructor(props) {
        super(props);

        this.state = {
            speed: 0,
            power: 0,
            distance: 0,
            time: '0:00:00',
            numericTime: 0
        }
    }

    componentDidMount() {
        setInterval(() => {
            this.fetchSpeed().then((res) => {
                return res.json()
            }).then(this.refreshData);
        }, timeInterval);
    }

    refreshData = (res) => {
        console.log("Current Speed: " + res.speed);
        console.log("Current Power: " + res.power);

        const shouldUpdateTime = res.speed > 2.5;

        this.setState({
            speed: shouldUpdateTime ? res.speed : 0,
            power: res.power,
            distance: this.integrateDistance(this.state.distance, timeInterval, res.speed),
            numericTime: shouldUpdateTime ? this.state.numericTime + timeInterval : this.state.numericTime,
            time: this.calculateTime(shouldUpdateTime ? this.state.numericTime + timeInterval : this.state.numericTime)
        });
    }

    calculateTime = (numericTime) => {
        numericTime /= 1000;
        const seconds = Math.floor(numericTime % 60);
        const minutes = Math.floor(numericTime / 60) % 60;
        const hours = Math.floor(numericTime / 60 / 60) % 24;

        const minuteZero = minutes < 10 ? "0" : "";
        const secondZero = seconds < 10 ? "0" : "";


        return `${hours}:${minuteZero}${minutes}:${secondZero}${seconds}`;
    }

    /*
     * oldDistance: Previous distance in mi
     * timeInterval: refresh time in ms
     * speed: speed in mph
     */
    integrateDistance = (oldDistance, timeInterval, speed) => {
        if (speed < 2.5) {
            return oldDistance;
        }

        return oldDistance + (speed / 60 / 60 / 1000) * timeInterval;
    }

    fetchSpeed() {
        return fetch('/data');
    }

    render() {
        return (
            <div>
                <Gauge
                    units='mph'
                    title='Speed'
                    value={this.state.speed}
                    maxValue={30}
                    stepValue={5}
                    subInterval={1}
                    width={150}
                    height={150} />
                <Gauge
                    units='Watts'
                    title='Power'
                    value={this.state.power}
                    maxValue={30}
                    stepValue={5}
                    subInterval={1}
                    width={150}
                    height={150} />

                <br/>

                <Numeric
                    units='mph'
                    measurement='Speed'
                    value={this.state.speed}
                    decimals={1}
                    width={150}
                    height={150} />
                <Numeric
                    measurement='Power'
                    units='W'
                    value={this.state.power}
                    decimals={1}
                    width={150}
                    height={150} />

                <br/>

                <Numeric
                    units='mi'
                    measurement='Distance'
                    value={this.state.distance}
                    decimals={2}
                    width={150}
                    height={150} />
                <Text
                    value={this.state.time}
                    measurement='Time'
                    width={150}
                    height={150} />

            </div>
        );
    }
}

export default DataComponents;